require('dotenv').config();
const logger = require('../../components/logger');
const {validDateFormat} = require('../../components/validators');

let parseAdamData = (json, parsedData) => {
    let parsedJson = json;
    if (validDateFormat( parsedData[0][process.env.DATE])) {
        const updatedDate = parsedData[0][process.env.DATE].split("/");
        parsedJson.Time_stamp = updatedDate[2] + "-"
            + updatedDate[1] + "-"
            + updatedDate[0] + "T"
            + parsedData[0][process.env.TIME_TO] + "Z";
    }
    parsedData.forEach(message => {
        try {
            parsedJson[message[process.env.HOSPITAL]]
                [message[process.env.GROUP]]
                [message[process.env.CONDITION]] = parseInt(message[process.env.COUNT]);
        } catch (error) {
            logger.error(error, error.name + "! " + error.message + " (" + message + ")");
        }
    });
    return parsedJson;
};

module.exports = {
    parseAdamData,
};