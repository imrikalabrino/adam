const { v4: uuidv4 } = require('uuid');
const dataTemplate = require('../../config/dataTemplate');
const hospitalCodes = require('../../config/hospitalCodes');

let InitDefaultJSON = (path) => {
    let env = 1;
    if (path.split("\\").pop().substring(0, 2) === "qa") {
        env = 0;
    }
    let defaultJSON = {
        GUID: uuidv4(),
        environment: env,
        Environment_num: 2,
        Time_stamp: new Date().toISOString(),
    };
    hospitalCodes.forEach(hospital => {
        defaultJSON[hospital] = JSON.parse(JSON.stringify(dataTemplate));
    });
    return defaultJSON;
};

module.exports = {
    InitDefaultJSON,
};