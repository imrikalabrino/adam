const fs = require ("fs");

module.exports = readAdamData = (path) => {
    const fileData = fs.readFileSync(path, 'utf8');
    if (fileData === '') {
        return false;
    }
    const dataFileLines = fileData.split("\r\n");
    const dataFileCells = dataFileLines.map(line => line.split(";"));
    return dataFileCells;
};
