require('dotenv').config();
const chokidar = require('chokidar');

module.exports = chokidar.watch(process.env.PATH_TO_VAULT, {
    ignored: /(^|[\/\\])\../,
    persistent: true,
});