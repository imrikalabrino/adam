require('dotenv').config();
const bunyan = require('bunyan');
const bFormat = require('bunyan-format');

const format = std => std.isTTY ? bFormat({outputMode: 'long', out: std}) : std;

module.exports = bunyan.createLogger({
    name: 'adam',
    src: true,
    streams: [
        {
            stream: format(process.stdout),
            level: process.env.LOG_LEVEL
        }
    ]
});