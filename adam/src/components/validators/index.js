require('dotenv').config();

let validChars = (string) => {
    const regExp = /([^A-Za-z])/g;
    return regExp.test(string);
};

let validDateFormat = (string) => {
    const regExp = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
    return regExp.test(string.toString());
};

module.exports = {
    validChars,
    validDateFormat,
};