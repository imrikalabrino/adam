require('dotenv').config();
const readAdamData = require('./src/components/reader');
const watcher = require('./src/components/watcher');
const logger = require('./src/components/logger/index');
const {parseAdamData} = require('./src/services/dataParser');
const {InitDefaultJSON} = require('./src/services/jsonInitializer');
const {validChars} = require('./src/components/validators');
const dgram = require('dgram');
const fs = require ("fs");
const server = dgram.createSocket('udp4');

watcher.on('add', (path => {
    let adamDataJSON = InitDefaultJSON(path);
    const file = readAdamData(path);
    if ((file || adamDataJSON.environment !== 0) && validChars(file)) {
        if (file) {
            adamDataJSON = parseAdamData(adamDataJSON, file);
        }
        console.log(adamDataJSON);
        logger.info(adamDataJSON.Time_stamp + " | " + adamDataJSON.GUID);
        const msg = JSON.stringify(adamDataJSON);
        server.send(msg, 0, msg.length, process.env.PORT, process.env.HOST);
    }
    try {
        fs.unlinkSync(path);
    } catch (error) {
        logger.error(error, error.name + "! " + error.message);
    }
}));